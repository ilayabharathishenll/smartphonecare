<?php
function mt_settings_page() {

    //must check that the user has the required capability 
    if (!current_user_can('manage_options'))
    {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    // variables for the field and option names 
    $hidden_field_name = 'mt_submit_hidden';

    // Read in existing option value from database
    $coupon_val = get_option( 'coupon_code' );
    $coupon_per = get_option( 'coupon_per_value' );
    $email_id = get_option( 'email_id' );
    $currency = get_option( 'currency','$' );
    $heading  = get_option( 'order_heading' );
	$sub_heading = get_option( 'order_sub_heading' );
	$innerpage_cost = get_option( 'order_innerpage_cost' );
	$quantity = get_option( 'quantity' );
	$paypal_id = get_option( 'paypal_id' );
	$paypal_currency = get_option( 'paypal_currency','USD' );
	$return_url = get_option( 'return_url' );
	$mail_subject = get_option( 'mail_subject' );
	$mail_content_user = get_option( 'mail_content_user' );
	$mail_content_admin = get_option( 'mail_content_admin' );
	$thanks_msg = get_option( 'thanks_msg' );
	$terms_text = get_option( 'terms_text' );
	$priceshow = get_option( 'priceshow','1' );
	$tax_per = get_option( 'tax_per' );
	$tax_name = get_option( 'tax_name' );
	$bg_color = get_option( 'bg_color','#0f9ccc' );
	$company_text = get_option( 'company_text' );
	$custom_css = get_option( 'custom_css' );
    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
        // Read their posted value
	    $coupon_val = $_POST['coupon_code_text'];
		$coupon_per = $_POST['coupon_code_percentage'];
	    $email_id = $_POST['email_id_text'];
	    $currency = $_POST['currency_text'];
		$heading  = $_POST['heading_text'];
		$sub_heading  = $_POST['sub_heading_text'];
		$innerpage_cost = $_POST['innerpage_cost'];
		$quantity =  $_POST['quantity'];
		$paypal_id = $_POST['paypal_id'];
		$paypal_currency = $_POST['paypal_currency'];
		$return_url = $_POST['return_url'];
		$mail_subject = $_POST['mail_subject'];
		$mail_content_user = $_POST['mail_content_user'];
		if($mail_content_user=="")
		{
			$mail_content_user ="Hi {name}, <br /><br />This is the confirmation email to inform you that we have received your request successfully.<br /><br />Order details are here<br /><br />{items}";
		}
		$mail_content_admin = $_POST['mail_content_admin'];
		{
			$mail_content_admin ="Hello Team, <br /><br />We have received new order from {name} and please analyse this and send confirmation mail asap. <br /><br />Order details are here<br /><br />{items}";
		}
		$thanks_msg = $_POST['thanks_msg'];
		$terms_text = $_POST['terms_text'];
		$priceshow = $_POST['priceshow'];
		$tax_per = $_POST['tax_per'];
		$tax_name = $_POST['tax_name'];
		$bg_color = $_POST['bg_color'];
		$company_text = $_POST['company_text'];
		$custom_css = $_POST['custom_css'];
		if($bg_color=="")
		$bg_color="#0f9ccc";
        // Save the posted value in the database
        update_option( 'coupon_code', $coupon_val );
		update_option( 'coupon_per_value', $coupon_per );
        update_option( 'email_id', $email_id );
        update_option( 'currency', $currency );
		update_option( 'order_heading', $heading );
		update_option( 'order_sub_heading', $sub_heading );
		update_option( 'order_innerpage_cost', $innerpage_cost );
		update_option( 'quantity', $quantity );
		update_option( 'paypal_id', $paypal_id );
		update_option( 'paypal_currency',$paypal_currency );
		update_option( 'return_url',$return_url );
		update_option( 'mail_subject',$mail_subject );		
		update_option( 'mail_content_user', $mail_content_user );
		update_option( 'mail_content_admin', $mail_content_admin );
		update_option( 'thanks_msg', $thanks_msg );
		update_option( 'terms_text', $terms_text );
		update_option( 'priceshow', $priceshow );
		update_option( 'tax_per', $tax_per );
		update_option( 'tax_name', $tax_name );
		update_option( 'bg_color', $bg_color );
		update_option( 'company_text', $company_text );
		update_option( 'custom_css', $custom_css );
        // Put an settings updated message on the screen

?>
<div class="updated"><p><strong><?php _e('settings saved.', 'order-now' ); ?></strong></p></div>
<?php

    }

    // Now display the settings editing screen

    echo '<div class="wrap">';

    // header

    echo "<h2>" . __( 'OrderNow Settings', 'order-now' ) . "</h2>";

    // settings form
    
    ?>

<form name="form1" method="post" action="">
<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
<p><label style="padding-right:73px;"><?php _e("Company/Service Name:  ", 'order-now' ); ?></label>
<input type="text" name="company_text" value="<?php echo  $company_text; ?>" size="20">
</p><hr />
<p><label style="padding-right:73px;"><?php _e("Email Id:  ", 'order-now' ); ?></label>
<input type="text" name="email_id_text" value="<?php echo  $email_id; ?>" size="20">
</p><hr />
<p><label style="padding-right:19px;"><?php _e("Currency Symbol: ", 'order-now' ); ?></label>
<input type="text" name="currency_text" value="<?php echo $currency; ?>" size="20">
Eg. $, &euro;, &cent;, &pound;
</p><hr />
<p>
<label style="padding-right:40px;"><?php _e("Coupon Code: ", 'order-now' ); ?></label> 
<input type="text" name="coupon_code_text" value="<?php echo $coupon_val; ?>" size="20">
<label style="padding-right:20px;"><?php _e("Coupon Discount Percentage: ", 'order-now' ); ?></label> 
<input type="text" name="coupon_code_percentage" value="<?php echo $coupon_per; ?>" size="20"> <?php _e("Fill this field if you have a coupon code(eg: 20): ", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:13px;"><?php _e("Paypal Business Id: ", 'order-now' ); ?></label> 
<input type="text" name="paypal_id" value="<?php echo $paypal_id; ?>" size="20"><?php _e("Fill this field if you want to show paypal button", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:25px;"><?php _e("Paypal Currency: ", 'order-now' ); ?></label> 
<input type="text" name="paypal_currency" value="<?php echo $paypal_currency; ?>" size="20"><?php _e("Refer this link for more info  https://developer.paypal.com/docs/classic/api/currency_codes/ ", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:60px;"><?php _e("Return Url: ", 'order-now' ); ?></label> 
<input type="text" name="return_url" value="<?php echo get_option('return_url'); ?>" size="80"/><?php _e("The url you want customers to return to after purchase", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:25px;"><?php _e("Inner Page Rate: ", 'order-now' ); ?></label> 
<input type="text" name="innerpage_cost" value="<?php echo $innerpage_cost; ?>" size="20"> <?php _e("Fill this field if you want to show inner page option", 'order-now' ); ?> 
</p><hr />
<label style="padding-right:55px;"><?php _e("Price Show: ", 'order-now' ); ?></label> 
<select name="priceshow" style="padding-right:20px;">
<option value="1" <?php if($priceshow=="1") echo "selected=selected"; ?>>Yes</option>
<option value="0" <?php if($priceshow=="0") echo "selected=selected"; ?>>No</option>
</select> <?php _e("Select this field if you want to show/hide price in front view", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:60px;"><?php _e("Tax Name: ", 'order-now' ); ?></label> 
<input type="text" name="tax_name" value="<?php echo $tax_name; ?>" size="20">
</p><hr />
<p>
<label style="padding-right:84px;"><?php _e("Tax %: ", 'order-now' ); ?></label> 
<input type="text" name="tax_per" value="<?php echo $tax_per; ?>" size="20">
</p><hr />
<p>
<label style="padding-right:65px;"><?php _e("Quantity: ", 'order-now' ); ?></label> 
<select name="quantity" style="padding-right:25px;">
<option value="1" <?php if($quantity=="1") echo "selected=selected"; ?>>Yes</option>
<option value="0" <?php if($quantity=="0") echo "selected=selected"; ?>>No</option>
</select> <?php _e("Select this field if you want to show quantity box in front view", 'order-now' ); ?> 
</p><hr />
<p>
<label style="padding-right:42px;"><?php _e("Select Form Color:",'order-now' ); ?> </label>
	<input class="color-field" id="color-field" type="text" name="bg_color" value="<?php echo $bg_color; ?>"/>
</p>
<p><label style="padding-right:57px;"><?php _e("Form Title:  ", 'order-now' ); ?></label>
<input type="text" name="heading_text" value="<?php echo stripslashes($heading); ?>" size="60">
</p><hr />
<p><label style="padding-right:42px;"><?php _e("Sub Heading:  ", 'order-now' ); ?></label>
<input type="text" name="sub_heading_text" value="<?php echo stripslashes($sub_heading); ?>" size="80">
</p><hr />
<p><label style="padding-right:42px;"><?php _e("Mail Subject:  ", 'order-now' ); ?></label>
<input type="text" name="mail_subject" value="<?php echo stripslashes($mail_subject); ?>" size="80">
</p><hr />
<p>
<label style="padding-right:42px;"><?php _e("Mail Template to user:  ", 'order-now' ); ?></label></p>
<p> <?php 
 $editor_settings = array( 'media_buttons' => false,'editor_height' => 200 );
 wp_editor( stripslashes($mail_content_user), 'mail_content_user',$editor_settings  ); ?>
</p>
Dont remove {items} and {name} it should replace customer name and order details.
<hr />
<p>
<label style="padding-right:42px;"><?php _e("Mail Template to admin:  ", 'order-now' ); ?></label></p>
<p>
 <?php wp_editor( stripslashes($mail_content_admin), 'mail_content_admin',$editor_settings ); ?>
</p>
Dont remove {items} and {name} it should replace customer name and order details.
<hr />
<label style="padding-right:42px;"><?php _e("Thanks page Message:  ", 'order-now' ); ?></label></p>
<p>
<textarea name="thanks_msg" cols="100" rows="5"><?php echo stripslashes($thanks_msg); ?></textarea>
</p><hr />
<label style="padding-right:42px;"><?php _e("Terms and conditions text appear above order now button:  ", 'order-now' ); ?></label></p>
<p>
<textarea name="terms_text" cols="100" rows="5"><?php echo stripslashes($terms_text); ?></textarea>
</p><hr />
<hr />
<label style="padding-right:42px;"><?php _e("Custom CSS", 'order-now' ); ?> : </label></p>
<p>
<textarea name="custom_css" cols="100" rows="5"><?php echo stripslashes($custom_css); ?></textarea>
</p><hr />
<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Settings') ?>" />
</p>
</form>
</div>
<?php
}