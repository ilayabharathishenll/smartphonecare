<?php
/*
Plugin Name: Responsive Order Form
Plugin URI: http://responsiveexpert.com/demo/wp/
Description: Awesome order form for Corporates, Freelancers, Web Developers.
Version: 1.1
Author: responsiveexpert
Author URI: http://responsiveexpert.com/
License: GPL
Text Domain: order-now
Domain Path: /languages/
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define('order_url', plugin_dir_url(__FILE__));
define('order_path', plugin_dir_path(__FILE__));
require_once order_path. 'settings.php';
function category_post_type() {
	register_post_type( 'form_category', 
		array(
				'labels' => array(
							'name' => __( 'Form Categories' ),
							'singular_name' => __( 'Form Category' ),
							'add_new' => __( 'Add New' ),
							'add_new_item' => __( 'Add New Category' ),
							'edit' => __( 'Edit' ),
							'edit_item' => __( 'Edit Category' ),
							'new_item' => __( 'New Category' ),
							'view' => __( 'View Category' ),
							'view_item' => __( 'View Category' ),
							'search_items' => __( 'Search Category' ),
							'not_found' => __( 'No category found' ),
							'not_found_in_trash' => __( 'No category found in Trash' ),
							'parent' => __( 'Parent Category' ),
							      ),
               'public' => true,
               'publicly_queryable' => true,
			   'show_in_menu' => false,
               'show_ui' => true,
               'query_var' => true,
               'rewrite' => true,
               'capability_type' => 'post',
               'hierarchical' => false,
               'menu_position' => 4,
               'supports' => array(
                             'title',
							 'thumbnail',
                             )
			  )
	  );
}
function add_sub_categories() {
	register_post_type( 'form_sub_category', 
			array(
					'labels' => array(
								'name' => __( 'Form Sub Categories' ),
								'singular_name' => __( 'Form Sub Category' ),
								'add_new' => __( 'Add New' ),
								'add_new_item' => __( 'Add New Sub Category' ),
								'edit' => __( 'Edit' ),
								'edit_item' => __( 'Edit Sub Category' ),
								'new_item' => __( 'New Sub Category' ),
								'view' => __( 'View Sub Category' ),
								'view_item' => __( 'View Sub Category' ),
								'search_items' => __( 'Search Sub Category' ),
								'not_found' => __( 'No Sub category found' ),
								'not_found_in_trash' => __( 'No Sub category found in Trash' ),
								'parent' => __( 'Parent Sub Category' ),
									  ),
				   'public' => true,
				   'publicly_queryable' => true,
				   'show_in_menu' => false,
				   'show_ui' => true,
				   'query_var' => true,
				   'rewrite' => true,
				   'capability_type' => 'post',
				   'hierarchical' => false,
				   'menu_position' => 4,
				   'supports' => array(
								 'title',
								 'thumbnail'
								 )
				  )
		  ); 
		  	   flush_rewrite_rules();
}
function add_sub_category_options() {
	register_post_type( 'form_options', 
			array(
					'labels' => array(
								'name' => __( 'Form Sub Category Options' ),
								'singular_name' => __( 'Form Sub Category Option' ),
								'add_new' => __( 'Add New' ),
								'add_new_item' => __( 'Add New Option' ),
								'edit' => __( 'Edit' ),
								'edit_item' => __( 'Edit Option' ),
								'new_item' => __( 'New Sub Category Option' ),
								'view' => __( 'View Sub Category Option' ),
								'view_item' => __( 'View Sub Category Option' ),
								'search_items' => __( 'Search Sub Category Option' ),
								'not_found' => __( 'No Sub Category Option found' ),
								'not_found_in_trash' => __( 'No Sub Category Option found in Trash' ),
								'parent' => __( 'Parent Sub Category Option' ),
									  ),
				   'public' => true,
				   'publicly_queryable' => true,
				   'show_in_menu' => false,
				   'show_ui' => true,
				   'query_var' => true,
				   'rewrite' => true,
				   'capability_type' => 'post',
				   'hierarchical' => false,
				   'menu_position' => 4,
				   'supports' => array(
								 'title',
								  'thumbnail',
								 )
				  )
		  ); 
	
}
function order_now(){
echo '<h1>Welcome to order now Plugin</h1>';
echo '<h2>Use the below tips to get started using Order Now Form</h2>';
echo '<h3>How To Build Your Form?</h3>';
echo '<p>First you should add categories or sub categories, if there is no sub category then there will be displaying just a contact form.  You can add extra options for each sub categories.</p>';
echo '<p> In settings, you can set email id (where you want to get order related emails) and default currency symbol, coupon code, coupon discount percentage, inner page rate, form title and sub titles etc. If there is no coupon code or coupon discount percentage then form will not show the coupon code option. And if there is no inner page rate then the from will not display the inner page option.</p>'; 
echo '<h3>Shortcode</h3>';
echo '<p>Place <b>[order_now_form]</b> in any area that accepts shortcodes to display your form anywhere you like.</p><p> Even in the middle of your page or posts content.</p>';
}
require_once order_path. 'metaboxes.php';
function save_custom_meta( $post_id ) {
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
  // check permissions
	if (isset( $_POST['post_type'] ) && ('page' == $_POST['post_type'])) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}
    if(isset($_POST['category_metabox']) && ($_POST['category_metabox'] != "")):
		update_post_meta($post_id, 'category_metabox', $_POST['category_metabox']);
	endif;	
	if(isset($_POST['sub_cat_price']) && ($_POST['sub_cat_price'] != "")):
		update_post_meta($post_id, 'sub_cat_price', $_POST['sub_cat_price']);
	endif;	
    if(isset($_POST['sub_cat_price']) && ($_POST['sub_cat_price'] != "")):
		update_post_meta($post_id, 'sub_cat_price', $_POST['sub_cat_price']);
	endif;	
	if(isset($_POST['sub_cat_boxstyle']) && ($_POST['sub_cat_boxstyle'] != "")):
		update_post_meta($post_id, 'sub_cat_boxstyle', $_POST['sub_cat_boxstyle']);
	endif;	
    if(isset($_POST['sub_cat_icon']) && ($_POST['sub_cat_icon'] != "")):
		update_post_meta($post_id, 'sub_cat_icon', $_POST['sub_cat_icon']);
	endif;
    if(isset($_POST['sub_cat_details']) && ($_POST['sub_cat_details'] != "")):
		update_post_meta($post_id, 'sub_cat_details', $_POST['sub_cat_details']);
	endif;
    if(isset($_POST['option_cat_price']) && ($_POST['option_cat_price'] != "")):
		update_post_meta($post_id, 'option_cat_price', $_POST['option_cat_price']);
	endif;
    if(isset($_POST['category_options_metabox']) && ($_POST['category_options_metabox'] != "")):
		update_post_meta($post_id, 'category_options_metabox', $_POST['category_options_metabox']);
	endif;
	if(isset($_POST['main_cat_icon']) && ($_POST['main_cat_icon'] != "")):
		update_post_meta($post_id, 'main_cat_icon', $_POST['main_cat_icon']);
	endif;
}
//Modify admin columns
function main_category_edit_columns($columns){
        $columns = array(  
            "cb" => "<input type=\"checkbox\" />",  
            "title" => __('Title'),
            "icon" => __('Icon Class'),
        );
        return $columns;  
}
function sub_category_edit_columns($columns){
        $columns = array(  
            "cb" => "<input type=\"checkbox\" />",  
            "title" => __('Title'),
			"category" => __('Category'),
			"box style" => __('Sub Category Box Style'),
            "price" => __('Price'),
            "icon" => __('Icon Class'),
			"details" => __('Details'),
        );
  
        return $columns;  
}
function sub_category_options_edit_columns($columns){
        $columns = array(  
            "cb" => "<input type=\"checkbox\" />",  
            "title" => __('Title'),
			"category" => __('Category'),
            "price" => __('Price'),
        );
        return $columns;  
}

//Content for admin columns
function sub_category_custom_columns($column){
	global $post;  
	$tmp_post = $post;
	switch ($column)  
	{  
		case "price":
			echo get_post_meta($post->ID, 'sub_cat_price',true);
		break;
		case "category":
			$cat_id = get_post_meta($post->ID, 'category_metabox',true);
			$args = array(
				'post_type' => 'form_category',
				'nopaging'  => true,
				'post_status'  => 'publish',
			);
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) : $query->the_post();
				if($cat_id==$post->ID)
				echo the_title();
			endwhile;	 
		break;
		case "box style":
			$cat_id = get_post_meta($post->ID, 'sub_cat_boxstyle',true);
			if($cat_id=="1")
				echo ''.__(" Package Type  ", 'order-now' ).'';
			else if($cat_id=="2")
				echo ''.__(" Item Type  ", 'order-now' ).'';
		break;
		case "icon":
			echo get_post_meta($post->ID, 'sub_cat_icon',true);
		break;
		case "details":
			echo get_post_meta($post->ID, 'sub_cat_details',true);
		break;
	}  
	$post = $tmp_post; 
}
function main_category_custom_columns($column){
	global $post;  
	$tmp_post = $post;
	switch ($column)  
	{  
		case "icon":
			echo get_post_meta($post->ID, 'main_cat_icon',true);
		break;
	}  
	$post = $tmp_post; 
}
function sub_category_coptions_custom_columns($column){
	global $post;  
	$tmp_post = $post;
	switch ($column)  
	{  
		case "category":
			$cat_id = get_post_meta($post->ID, 'category_options_metabox',true);
			$args = array(
				'post_type' => 'form_sub_category',
				'nopaging'  => true,
				'post_status'  => 'publish',
			);
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) : $query->the_post();
				if($cat_id==$post->ID)
				echo the_title();
			endwhile;	 
		break;
		case "price":
			echo get_post_meta($post->ID, 'option_cat_price',true);
		break;
	}  
	$post = $tmp_post; 
}
function order_plugin_init() {
   load_plugin_textdomain( 'order-now', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'order_plugin_init' );
add_action( 'save_post', 'save_custom_meta');
add_action( 'init', 'category_post_type' );
add_action( 'init', 'add_sub_categories' );
add_action( 'init', 'add_sub_category_options' );
add_action( 'add_meta_boxes', 'sub_category_metabox_fun' );
add_action( 'add_meta_boxes', 'price_category_metabox_fun' );
add_action( 'add_meta_boxes', 'icon_category_metabox_fun' );
add_action( 'add_meta_boxes', 'display_sub_category_boxstyle_meta_box_fun' );
add_action( 'add_meta_boxes', 'details_category_metabox_fun' );
add_action( 'add_meta_boxes', 'icon_main_category_metabox_fun' );
add_action( 'add_meta_boxes', 'option_category_price_metabox_fun' );
add_action( 'add_meta_boxes', 'option_sub_category_metabox_fun' );

add_filter('manage_form_sub_category_posts_columns', 'sub_category_edit_columns');
add_action('manage_form_sub_category_posts_custom_column', 'sub_category_custom_columns', 10, 2);

add_filter('manage_form_main_category_posts_columns', 'main_category_edit_columns');
add_action('manage_form_main_category_posts_custom_column', 'main_category_custom_columns', 10, 2);

add_filter('manage_form_options_posts_columns', 'sub_category_options_edit_columns');
add_action('manage_form_options_posts_custom_column', 'sub_category_coptions_custom_columns', 10, 2);

add_action( 'admin_menu', 'order_now_menu' );
add_shortcode('order_now_form', 'ordernow_shortcode');
add_action('wp_head','orderform_ajaxurl');
function orderform_ajaxurl() {
?>
<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}
function ordernow_shortcode($atts){
	global $post;  
	$email = get_option('email_id');
	$coupon_code = get_option('coupon_code');
	$coupon_per_value = get_option('coupon_per_value');
	$order_innerpage_cost = get_option('order_innerpage_cost');
	$order_head = get_option( 'order_heading' );
	$order_sub_head = get_option( 'order_sub_heading' );
	$terms_text = get_option( 'terms_text' );	
	$quantity = get_option( 'quantity' );
	$priceshow = get_option( 'priceshow' );
	wp_register_style('bootstrap-style', order_url. 'css/bootstrap.css');
	wp_enqueue_style('bootstrap-style');
	wp_register_style('custom-style', order_url. 'css/style.css');
	wp_enqueue_style('custom-style');
	wp_register_style('font-awesome', order_url.'font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('font-awesome');
	wp_register_style('google-font1','http://fonts.googleapis.com/css?family=Arvo:400,400italic,700,700italic'); 
	wp_enqueue_style('google-font1');
	wp_register_style('google-font2','http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');
	wp_enqueue_script('jquery');
	wp_register_script('bootstrap', order_url.'js/bootstrap.js');
	wp_enqueue_script('bootstrap');
	wp_register_script('js_orderform', order_url.'js/orderform.js');
	wp_enqueue_script('js_orderform');
	wp_register_script('js_core', order_url.'js/core.js');
	wp_enqueue_script('js_core');
	$output = '<!-- Order Form Area Start -->
	<div id="holderdiv" class="clearfix">
    <form action="#" method="post" id="reo-order-frm" enctype="multipart/form-data">
    <section class="reo-order-form">
    	<div class="container text-center">';
		if($order_head!="")
		{
        	$output.= '<h2>'.$order_head.'</h2>';
		}
		if($order_sub_head!="")
		{
			$output.= '<p class="medium-txt">'.$order_sub_head.'</p>';
		}
        $output.= '<!-- Order Buttons Start -->
            <div class="reo-order-btns clearfix">';
			$args = array(
				'post_type' => 'form_category',
				'nopaging'  => true,
				'order'     => 'ASC',
				'post_status'  => 'publish',
			);
			$query = new WP_Query( $args );
			$count=0;
			$cust_choice ="";
			$cnt_first=0;
			while ( $query->have_posts() ) : $query->the_post();
				$cnt_first = $cnt_first +1;
			endwhile;
			if($cnt_first>0)
			{			
			while ( $query->have_posts() ) : $query->the_post();
			$count=$count+1;
			if($count==1)
			{
			 	$active=" active";
				$cust_choice = get_the_title( $post->ID );
			}
			else
			{
				 $active="";
			}
            $icon_class = get_post_meta($post->ID , 'main_cat_icon',true);  
		    $output.='<div class="col-md-4">
                      <a class="reo-order-opt-link'.$active.'" data-param1="'.$post->ID.'" data-param2="'.get_the_title( $post->ID ).'">
                        <span class="icon">';
						if(has_post_thumbnail())
						{
							$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID,'thumbnail'), 'full' );
							$featimage = $imageid['0'];
							$printable_url = '<img src="' . $featimage . '">';
							$output.=$printable_url;
						}
						else if($icon_class)
						{
							$output.='<i class="'.$icon_class.'"></i>';
						}
                        $output.='</span><span>'.get_the_title( $post->ID ).'</span>
                    </a>
                    </div>';
			endwhile;	
			}
            $output.='</div>
            <!-- Order Buttons End -->
            
            <!-- Order Form Categories Start -->
            <div class="reo-order-catogories clearfix">           
            <!-- Order section Start -->
                <div class="col-md-8 clearfix">
                 <div class="row">';                     
                      $output.='<div class="col-md-12 cms-category" id="reo-cms-main-category">
                        	<div class="row" id="reo-result">
						    </div>
                        </div>
                        <!-- CMS Services End -->
                        
                        <div class="col-md-12">';
                        $output.=' <!-- Additional Options Start -->
                        	<div id="reo-add_options">
                            	<ul class="reo-list-opt clearfix" id="reo-list-opt">
								   
					            </ul>
                            </div>   <!-- Additional Options End --> ';
		                    $output.='<!-- Form Start -->
                            <div class="submit-opts-form clearfix">
							 <div class="reo-controls" id="error_order" style="color:red;"></div>
                            	<div class="clearfix">
								    <input type="text" name="customer_choice" value="'.$cust_choice.'" style="display: none;"/>
									<input type="text" name="customer_sub_choice" style="display: none;"/>
									<input type="text" name="type_cost" style="display: none;"/>
									<input type="text" name="actual_amt" style="display:none;"/>																																			
									<input type="text" name="order_total_amt" style="display: none;"/>	
									<input type="text" name="innerpage_cost" style="display: none;"/>	
                                    <input type="text" placeholder="'. __(" Your Name * :  ", 'order-now' ).'"  name="customer_name" class="first-field">
                                    <input type="text" placeholder="'. __(" Your Email * :  ", 'order-now' ).'" name="customer_email" class="second-field">
                                    <input type="text" placeholder="'. __(" Your Phone :  ", 'order-now' ).'" name="customer_contact"  class="third-field">
                                    <textarea cols="10" rows="5" name="customer_message" placeholder="Message *" class="forth-field"></textarea>									
                                </div>
								<!-- Order Concept Area Start -->
								<div class="reo-orderbtn-area" id="reo-order-concept-area">'.$terms_text.'<div class="order-btn-cont"><a href="#" class="button" id="order_btn_id2">'. __("Order Now !",'order-now').'</a></div>
								</div>
								<!-- Order Concept Area End -->';						
								$output.='</div>
                            <!-- Form End -->
                        </div>
                    </div>
                </div><!-- Order Section End -->';
			if($cnt_first>0)
			{	
    	    $output.='<!-- Order Summary Start -->
                <div class="col-md-4 clearfix" id="reo-order-summary-box">
                	<div class="reo-summary-box">
                    	<div class="heading-total">'. __(" Order Summary :  ", 'order-now' );
						if($priceshow=="1")
						{
						 $output.='<span class="color-txt" id="order_total">';
						}
						$output.='</div><!-- Package Features Start -->
                        <div class="reo-summary-basic-pack" id="summary-pack">
                            <h5></h5>
                            <ul class="reo-pack-in" id="reo-pack-in">
							<li></li>
                            </ul>
                        </div>
						<!-- Package Features End -->';
						if($order_innerpage_cost!="")
						{
						$output.='<!-- Innerpage count section Start -->
                        <div class="reo-pages-area-cal">
                        	<ul class="page-ul">
                            	<li>'. __(" Inner Pages ", 'order-now' ).'</li>
                            	<li><input type="text" class="pages-txtbx" name="conversion_inner_pages" placeholder="0"></li>
                            </ul>
                        </div>
						<!-- Innerpage count section End -->';
						}
						if($quantity=="1")
						{
						$output.='<!-- Quantity section Start -->
                        <div class="reo-pages-area-cal">
                        	<ul class="page-ul">
                            	<li style="padding-right:25px;">'. __("Quantity", 'order-now' ).'</li>
                            	<li><input type="text" class="pages-txtbx" name="item_quantity" placeholder="1"></li>
                            </ul>
                        </div>
						<!-- Innerpage count section End -->';
						}						
						$output.='<!-- Extra option selected section Start -->
                        <div class="reo-summary-basic-pack">
                            <h5>'. __(" Extra Options ", 'order-now' ).'</h5>
                            <ul class="pack-add" id="pack-add">
							<li id="nothing">'. __(" No Option Selected ", 'order-now' ).'</li>
                            </ul>
                        </div>
						<!-- Extra option selected section End -->';
						if($priceshow=="1")
						{
							if(($coupon_code)&&($coupon_per_value))
							{
							$output.='<!-- Discount coupon section Start -->
							<div class="reo-coupon-box">
								<h5><span><i class="fa fa-gift"></i></span>'. __(" Have a Discount Coupon ? ", 'order-now' ).'</p></h5>
								<input type="text" name="coupon_text" id="coupon_text" class="coupon-txtbx" placeholder="'.__(" Enter Code ", 'order-now' ).'">
								<a id="discnt_btn_id">'.__(" Apply ! ", 'order-now' ).'</a>
								<p id="dis_price"></p>
								 <input type="hidden" name="discount_text" id="discount_text">
							</div><!-- Discount coupon section End -->';
							}
						}
						$output.='<!-- Main order button area Start -->
                        <div class="reo-orderbtn-area">'.$terms_text.'<div class="order-btn-cont"><a href="" class="button" id="order_btn_id">'. __("Order Now !",'order-now').'</a></div>
                        </div>
						<!-- Main order button area end -->
                    </div>
                </div>
            	<!-- Order Summary End -->';
			}
             $output.='</div>
            <!-- Order Form Categories End -->
        </div>
    </section>
	</form>
	</div>
    <!-- =================================================
    Order Content Area End -->';
	return $output;
}
function category_ajax_request() {
	global $post;  
	$priceshow =get_option( 'priceshow' );
	// The $_REQUEST contains all the data sent via ajax 
	if ( isset($_REQUEST) ) {
	$categoryid = $_REQUEST['category'];
	$args = array(
	'meta_key'     => 'category_metabox',
	'meta_value'   => $categoryid,
	'meta_compare' => '=',
	'post_type'    => 'form_sub_category',
	'orderby'      =>  'post_date',
    'order'        =>  'ASC',
	'post_status'  => 'publish',
	);
	//echo $categoryid;
	$query = new WP_Query( $args );
	$cnt=0;
	$a_count=0;
	while ( $query->have_posts() ) : $query->the_post();
		$cnt = $cnt +1;
	endwhile;
	if($cnt==0)
	{
	 //echo '';
	}
	while ( $query->have_posts() ) : $query->the_post();
		$box_style = get_post_meta($post->ID , 'sub_cat_boxstyle',true);
		if($box_style=="1")
		{
			$a_count = $a_count+1;
			if($a_count==1)
			$active = " active";
			else
			$active = "";
			echo '<div class="col-md-6">
					 <div class="html5-web">
					  <div class="normal-pk" data-param1="'.$post->ID.'" data-param2="'.get_the_title( $post->ID ).'">
						 <h3>'.get_the_title( $post->ID ).'</h3>';
						if(has_post_thumbnail())
						{
							$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID,'thumbnail'), 'full' );
							$featimage = $imageid['0'];
							$printable_url = '<img src="' . $featimage . '">';
							echo $printable_url;
						}
						if($priceshow=="1")
						{
						 echo '<div class="price"><span>'.get_option( 'currency' ).'</span>'.get_post_meta($post->ID , 'sub_cat_price',true).'</div>';
						}						
					echo get_post_meta($post->ID , 'sub_cat_details',true).'</div>
				   </div>
				</div>';
		}
		else
		{
			$a_count = $a_count+1;
			if($a_count==1)
			$active = " active";
			else
			$active = "";
			echo '<div class="col-md-3">
				<div class="cms-cont" data-param1="'.$post->ID.'" data-param2="'.get_the_title( $post->ID ).'">';
				if(has_post_thumbnail())
				{
					$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID,'thumbnail'), 'full' );
					$featimage = $imageid['0'];
					$printable_url = '<img src="' . $featimage . '">';
					echo $printable_url;
				}
				else if(get_post_meta($post->ID , 'sub_cat_icon',true))
				{
			     	echo '<i class="'.get_post_meta($post->ID , 'sub_cat_icon',true).'"></i>';
				}
				echo'<h5>'.get_the_title( $post->ID ).'</h5>
					<p id="wrdpress_pkg"><span>'.get_option( 'currency' ).'</span>'.get_post_meta($post->ID , 'sub_cat_price',true).'</p>
			 </div>
			</div>';
		}
	endwhile;
	}
	die();
}
function option_ajax_request() {
	global $post;  
	$priceshow =get_option( 'priceshow' );
	// The $_REQUEST contains all the data sent via ajax 
	if ( isset($_REQUEST) ) {
	$sub_categoryid = $_REQUEST['sub_category'];
	$args = array(
	'meta_key'     => 'category_options_metabox',
	'meta_value'   => $sub_categoryid,
	'meta_compare' => '=',
	'post_type'    => 'form_options',
	'orderby'      => 'post_date',
    'order'        => 'ASC',
	'post_status'  => 'publish',
	);
	//echo $categoryid;
	$query = new WP_Query( $args );
	while ( $query->have_posts() ) : $query->the_post();
	if($priceshow=="1")
	{
		echo '<li>
           <input type="checkbox" id="option'.$post->ID.'" value="'.get_the_title( $post->ID ).'<span>['.get_option( 'currency' ).get_post_meta($post->ID , 'option_cat_price',true).']</span>" data="'.get_post_meta($post->ID , 'option_cat_price',true).'" name="html_options[]" class="check-opt">'.get_the_title( $post->ID ).' <span>['.get_option( 'currency' ).get_post_meta($post->ID , 'option_cat_price',true).']</span>
          </li>';
	}
	else
	{
		echo '<li>
           <input type="checkbox" id="option'.$post->ID.'" value="'.get_the_title( $post->ID ).'" data="'.get_post_meta($post->ID , 'option_cat_price',true).'" name="html_options[]" class="check-opt">'.get_the_title( $post->ID ).
          '</li>';
	}
	endwhile;
	}
  die();
}
function sub_category_details_ajax_request() {
	global $post;  
	// The $_REQUEST contains all the data sent via ajax 
	if ( isset($_REQUEST) ) {
	$sub_categoryid = $_REQUEST['sub_category'];
	$args = array(
	'post_type'    => 'form_sub_category',
	'orderby'      => 'post_date',
    'order'        => 'ASC',
	'post_status'  => 'publish',
	);
	//echo $categoryid;
	$query = new WP_Query( $args );
	
	while ( $query->have_posts() ) : $query->the_post();
    if($post->ID == $sub_categoryid)
	{
		$sub_category_details = get_post_meta($post->ID , 'sub_cat_details',true);
		$sub_category_price = get_post_meta($post->ID , 'sub_cat_price',true);
	}
	endwhile;
	echo $sub_category_details.'@'.$sub_category_price;
//	echo '@'.$sub_category_price;
	}
  die();
}
function get_first_category_ajax_request() {
	global $post;  
	$priceshow =get_option( 'priceshow' );
	$categoryid = "";
	$args = array(
	'post_type'    => 'form_category',
	'orderby'      => 'post_date',
    'order'        => 'ASC',
	'post_status'  => 'publish',
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) : $query->the_post();
	$categoryid = $post->ID;
	$main_cat = get_the_title( $post->ID );
	echo $main_cat."||";
	$args = array(
	'meta_key'     => 'category_metabox',
	'meta_value'   => $categoryid,
	'meta_compare' => '=',
	'post_type'    => 'form_sub_category',
	'orderby'      => 'post_date',
    'order'        => 'ASC',
	'post_status'  => 'publish',
	);
	$query = new WP_Query( $args );
	$cnt=0;
	$a_count=0;
	while ( $query->have_posts() ) : $query->the_post();
		$cnt = $cnt +1;
	endwhile;
	while ( $query->have_posts() ) : $query->the_post();
		$box_style = get_post_meta($post->ID , 'sub_cat_boxstyle',true);
		if($box_style=="1")
		{
			$a_count = $a_count+1;
			if($a_count==1)
			$active = " active";
			else
			$active = "";
			echo '<div class="col-md-6">
					 <div class="html5-web">
					  <div class="normal-pk" data-param1="'.$post->ID.'" data-param2="'.get_the_title( $post->ID ).'">
						 <h3>'.get_the_title( $post->ID ).'</h3>';
						if(has_post_thumbnail())
						{
							$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID,'thumbnail'), 'full' );
							$featimage = $imageid['0'];
							$printable_url = '<img src="' . $featimage . '">';
							echo $printable_url;
						}
						if($priceshow=="1")
						{
						  echo '<div class="price"><span>'.get_option( 'currency' ).'</span>'.get_post_meta($post->ID , 'sub_cat_price',true).'</div>';
						}
						echo get_post_meta($post->ID , 'sub_cat_details',true).'
					</div>
				   </div>
				</div>';
		}
		else
		{
			$a_count = $a_count+1;
			if($a_count==1)
			$active = " active";
			else
			$active = "";
			echo '<div class="col-md-3">
				<div class="cms-cont" data-param1="'.$post->ID.'" data-param2="'.get_the_title( $post->ID ).'">
					<h5>'.get_the_title( $post->ID ).'</h5>';
					if(has_post_thumbnail())
					{
						$imageid = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID,'thumbnail'), 'full' );
						$featimage = $imageid['0'];
						$printable_url = '<img src="' . $featimage . '">';
						echo $printable_url;
					}
					else if(get_post_meta($post->ID , 'sub_cat_icon',true))
					{
						echo '<i class="'.get_post_meta($post->ID , 'sub_cat_icon',true).'"></i>';
					}
					if($priceshow=="1")
					{
					echo '<p id="wrdpress_pkg"><span>'.get_option( 'currency' ).'</span>'.get_post_meta($post->ID , 'sub_cat_price',true).'</p>';
					}
			 echo'</div>
			</div>';
		}
	endwhile;
	endif;
	die();
}
function get_currency_ajax_request() {
 echo get_option( 'currency' );
 die();
}
function innerpage_ajax_request() {
 echo get_option( 'order_innerpage_cost' );
 die();
}
function coupon_ajax_request() {
	$coupon_code = get_option('coupon_code');
	$coupon_per_value = get_option('coupon_per_value');
	$currency = get_option( 'currency' );
	echo $coupon_code.'|'.$coupon_per_value.'|'.$currency;
	die();
}
function order_form_ajax_request() {
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$contact = $_REQUEST['contact'];
	$message = $_REQUEST['message'];
	$customer_choice = $_REQUEST['customerchoice'];
	$customer_sub_choice = $_REQUEST['customersubchoice'];
	$typecost = $_REQUEST['typecost'];
	$currency = get_option( 'currency' );
	$ordertotalamt = $_REQUEST['ordertotalamt'];
	$actualamt = $_REQUEST['actualamt'];
	if(isset($_REQUEST['discount_text']))
	$discount_text = $_REQUEST['discount_text'];
	else
	$discount_text = "";
	$priceshow =get_option( 'priceshow' );
	if(isset($_REQUEST['couponvalue']))
	{
		$couponvalue = $_REQUEST['couponvalue'];
	}
	else
	{
		$couponvalue = "";
	}
	$other_options = "";
	if(isset($_REQUEST['otheroptions']))
	{
		$otheroptions = $_REQUEST['otheroptions'];
	}
	else
	{
		$otheroptions ="";
	}
	$conversion_inner_pages = $_REQUEST['conversion_inner_pages'];
	$total_innerpage_cost = $_REQUEST['innerpage_cost'];
	$item_quantity = $_REQUEST['item_quantity'];
	$order_innerpage_cost = get_option('order_innerpage_cost');
	$paypal_id = get_option('paypal_id');
	$paypal_currency = get_option('paypal_currency');
	$coupon_per = get_option('coupon_per_value');	
	$return_url = get_option( 'return_url' );
	$site_url = get_bloginfo('url'); 
	
	$extra_options = "";
	$all_innerpage_cost = "";
	$pay_input_v = "";
	$i=1;  		
	if($paypal_currency=="")
	{
		$paypal_currency="USD";
	}
    echo '<section class="reo-order-form">
        <div class="container text-center">
            <div class="col-md-12">
			<form action="" method="post" target="_top" id="reo-order-pay-frm" enctype="multipart/form-data">
                <!-- Revise order contents start-->
                <div class="reo-addi-opts">
                    <h2 align="center">'. __("Please revise your order", 'order-now' ).'</h2>
                    <hr>
                    <ul class="reo-list-opt clearfix">
                        <li><strong>'. __("Name", 'order-now' ).' :</strong>'.$name.'</li>
                        <li><strong>'. __("Email", 'order-now' ).' :</strong>'.$email.'</li>
                        <li><strong>'. __("Contact", 'order-now' ).' :</strong>'.$contact.'</li>
                        <li><strong>'. __("Message", 'order-now' ).':</strong>'.$message.'</li>';
						if($priceshow=="1")
						{
                     		echo '<li><strong>'.$customer_choice." - ".$customer_sub_choice.' :</strong>'.$currency.$typecost.'</li>';
						}
						else
						{
							echo '<li><strong>'.$customer_choice." - ".$customer_sub_choice.'</strong></li>';

						}
						echo '<li><strong>'. __("Quantity", 'order-now' ).':</strong>'.$item_quantity.'</li>
					</ul>';					
					if($otheroptions!=""){
						  echo '<ul class="reo-list-confirm"><li><strong>'. __("Additional Options ", 'order-now' ).':</strong> <br/><li>';
						
						  foreach ($otheroptions as $value) {	
						  	$i++;	
							if($priceshow=="1")
							{					
							$f_value[$i]=explode('[',$value);
							$s_value[$i]=explode(']',$f_value[$i][1]);
							$pay_input_v.='<input type="hidden" name="item_name_'.$i.'" value="'.trim($f_value[$i][0],"<span>").'">
								<input type="hidden" name="amount_'.$i.'" value="'.$s_value[$i][0].'">
								<input type="hidden" name="quantity_'.$i.'" value="'.$item_quantity.'">';
							}
							echo '<li>'.$value.'</li>';
							$extra_options.=",".$value;					
						  }
						  $pay_input_v.='<input type="hidden" name="item_number" value="'.$i.'">';
						  echo '</ul>';
						  $extra_options = trim($extra_options,',');
					   }
					   if($total_innerpage_cost!="")
					   {
						   echo '<ul class="reo-list-opt clearfix">
						   <li><strong>'. __("Inner page cost", 'order-now' ).' :</strong>'.$currency.$total_innerpage_cost.' ('.$conversion_inner_pages.'*'.$currency.$order_innerpage_cost.')</li></ul>';
						   $all_innerpage_cost= $currency.$total_innerpage_cost.' ('.$conversion_inner_pages.'*'.$currency.$order_innerpage_cost.')';
						   $i=$i+1;
						  $pay_input_v.= '<input type="hidden" name="item_name_'.$i.'" value="Inner pages cost">
								<input type="hidden" name="amount_'.$i.'" value="'.$total_innerpage_cost.'">
								<input type="hidden" name="quantity_'.$i.'" value="'.$item_quantity.'">';
					   }
					if($priceshow=="1")
					{
						echo '<hr><div class="price-final">';
						if($actualamt!=""){
							echo '<h3>'. __("Grand Total  ", 'order-now' ).': '.$currency.$actualamt.'</h3>'; 
							echo '<h4>'. __("Amount after discount   ", 'order-now' ).':'.$currency.$ordertotalamt.'</h4>';} 
							else
							{                     
								echo '<h3>'. __("Grand Total   ", 'order-now' ).':'.$currency.$ordertotalamt.'</h3>';
							}
						echo '</div>';
					}
                echo '</div>
                <!-- Revise order contents end-->
                <!-- Button area start-->
                <div class="order-btn-cont pay-page">
                    <!-- Mail sending button area start-->
                    <input type="hidden" name="customer_name" value="'.$name.'">
                    <input type="hidden" name="customer_email" value="'.$email.'">
                    <input type="hidden" name="customer_contact" value="'.$contact.'">
                    <input type="hidden" name="customer_message" value="'.$message.'">
                    <input type="hidden" name="type_cost" value="'.$typecost.'">
                    <input type="hidden" name="tot_options" value="'.$extra_options.'">
                    <input type="hidden" name="customer_choice" value="'.$customer_choice.'">
                    <input type="hidden" name="sub_type" value="'.$customer_sub_choice.'">
					<input type="hidden" name="item_quantity" value="'.$item_quantity.'">       
                    <input type="hidden" name="inner_pages_cost" value="'.$all_innerpage_cost.'">                  
        			<input type="hidden" name="coupon_text" value="'.$couponvalue.'">
					<input type="hidden" name="discount_text" value="'.$discount_text.'">
					<input type="hidden" name="site_url" value="'.$site_url.'">';					
					if($actualamt!="")
					{						
						echo '<input type="hidden" name="total_cost" value="'.$actualamt.'">
						      <input type="hidden" name="discounted_cost" value="'.$ordertotalamt.'">';
					}
					else
					{
						echo '<input type="hidden" name="total_cost" value="'.$ordertotalamt.'">
						      <input type="hidden" name="discounted_cost" value="">';
					}
                    echo'<input type="submit" class="button" name="mailus" value="'. __("Mail Me !", 'order-now' ).'" id="order-mail-id">
                     <!-- Mail sending button area end-->';	
					 if($priceshow=="1")
					 {		
						 if($paypal_id!="")
						 {
						echo '<!-- Pay now button area start-->
						<input type="hidden" name="cmd" value="_cart" />
						<input type="hidden" name="upload" value="1">
						<input type="hidden" name="business" value="'.$paypal_id.'">
						<input type="hidden" name="lc" value="IN">
						<input type="hidden" name="item_name_1" value="'.$customer_choice. " ".$customer_sub_choice.'">
						<input type="hidden" name="amount_1" value="'.$typecost.'">
						<input type="hidden" name="quantity_1" value="'.$item_quantity.'">
						<input type="hidden" name="Name" value="">
						<input type="hidden" name="custom" id="custom" value="">';
						if($actualamt!="")
						{
						echo '<input type="text" name="discount_rate_cart" value="'.$coupon_per.'" style="display: none;">';
						}
						echo $pay_input_v;
						echo '<input type="hidden" name="currency_code" value="'.$paypal_currency.'">  
						<input type="hidden" name="return" value="' . $return_url . '">
						<input name="notify_url" value="' . plugin_dir_url(__FILE__) . 'ipn.php" type="hidden"> 
						<input type="hidden" name="no_note" value="0">
						<input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHostedGuest">
						<input type="submit" class="button" name="Pay Now" id="order_pay_id" value="'. __("Pay Now !", 'order-now' ).'" alt="PayPal – The safer, easier way to pay online.">
					   <!-- Pay now button area end-->';
						 }
                	 echo' </div>';
					 }
               echo'<!-- Button area end-->		
				</form>
            </div>
        </div>
	</section>';
	die();
}
function order_form_ajax_deliver_mail()
{
	$couponvalue ="";
	$customer_name = sanitize_text_field($_REQUEST['name']);
	$customer_email = sanitize_email($_REQUEST['email']);
	$customer_contact = sanitize_text_field($_REQUEST['contact']);
	$customer_message = esc_textarea($_REQUEST['message']);
	$customer_choice = $_REQUEST['customerchoice'];
	$customer_sub_choice = $_REQUEST['customersubchoice'];
	$typecost = $_REQUEST['typecost'];
	$currency = get_option( 'currency' );
	$ordertotalamt = $_REQUEST['ordertotalamt'];
	$disamount = $_REQUEST['discounted_cost'];
	$couponvalue = $_REQUEST['couponvalue'];
	$discount_text = $_REQUEST['discount_text'];
	$priceshow =get_option( 'priceshow' );
	$other_options = "";
	if(isset($_REQUEST['otheroptions']))
	{
		$otheroptions = $_REQUEST['otheroptions'];
	}
	else
	{
		$otheroptions[0] = "";
	}
	$opt_count = count($otheroptions);
	$inner_pages_cost = $_REQUEST['inner_pages_cost'];
	$item_quantity = $_REQUEST['item_quantity'];
	$mail_content_user = get_option( 'mail_content_user' );
	$mail_content_admin = get_option( 'mail_content_admin' );
	$thanks_msg = get_option( 'thanks_msg' );
	for($k=0;$k<$opt_count;$k++)
	{
		$other_options.=$otheroptions[$k].",";
	}
	if(isset($other_options))
	{
		$other_options = trim($other_options,",");
	}
	$to = get_option('email_id');
	if($to=="")
	{
		$to = "info@mywebsite.com";
	}
	$file = order_path. '/formcount/countforms.txt';
	$form_id = file_get_contents( $file ) + 1;
	$fp = fopen($file, "w");
	fputs ($fp, "$form_id");
	fclose($fp); 
	$order_heading = get_option('order_heading');
	$subject = "Order confirmation from ".$order_heading;
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers.= "From: $customer_name  <$customer_email>" . "\r\n";
	$headers_customer = "MIME-Version: 1.0" . "\r\n";
	$headers_customer.= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers_customer.= "From: admin  <$to>" . "\r\n";
	// If email has been process for sending, display a success message
	$message='Hi '. $customer_name.', <br /><br />';
	if($mail_content_user=="")
	{
		$message.="This is the confirmation email to inform you that we have received your request successfully.<br /><br />";
	}
	else
	{
		$message.=$mail_content_user.'<br /><br />';
	}
	$items='<table width="60%" border="1" cellspacing="0">';
	$items.='<tr><td colspan="2" align="center" bgcolor="#d6d6d6">'. __("CONTACT DETAILS ", 'order-now' ).'</td></tr>';
	$items.='<tr><td>'. __("Customer Name ", 'order-now' ).'</td><td>'.$customer_name.'</td></tr>';
	$items.='<tr><td>'. __("Customer Email ", 'order-now' ).'</td><td>'.$customer_email.'</td></tr>';
	$items.='<tr><td>'. __("Customer Contact ", 'order-now' ).'</td><td>'.$customer_contact.'</td></tr>';
	$items.='<tr><td>'. __("Customer Message ", 'order-now' ).'</td><td>'.$customer_message.'</td></tr>';
	$items.='<tr><td>'. __("Reference No ", 'order-now' ).'</td><td>#'.$form_id.'</td></tr>';
	if($customer_sub_choice!="")
	{
		$items.='<tr><td colspan="2" align="center" bgcolor="#d6d6d6">'. __("ITEM DETAILS ", 'order-now' ).'</td></tr>';
		if($priceshow=="1")
		{
			$items.='<tr><td>'. __("Customer Choice ", 'order-now' ).'</td><td>'.$customer_choice." - ".$customer_sub_choice." : ".$currency.$typecost.'</td></tr>';
		}
		else
		{
			$items.='<tr><td>'. __("Customer Choice ", 'order-now' ).'</td><td>'.$customer_choice." - ".$customer_sub_choice.'</td></tr>';
		}
		if($inner_pages_cost!="")
		{
			$items.='<tr><td>'. __("Inner Pages ", 'order-now' ).'</td><td>'.$inner_pages_cost.'</td></tr>';
		}
		if($other_options!="")
		{
			$items.='<tr><td>'. __("Extra Addons ", 'order-now' ).'</td><td>'.$other_options.'</td></tr>';
		}
		if($item_quantity!="")
		{
			$items.='<tr><td>'. __("Quantity", 'order-now' ).'</td><td>'.$item_quantity.'</td></tr>';
		}
		if($priceshow=="1")
		{
			if($couponvalue!="")
			{
				$items.='<tr><td><b>'. __("Total Cost ", 'order-now' ).'</b></td><td><b>'.$currency.$ordertotalamt.'</b></td></tr>';
				$items.='<tr><td><b>'. __("Amount after discount   ", 'order-now' ).'</b></td><td><b>'.$currency.$disamount.'</b></td></tr>';
			}
			else
			{	
			$items.='<tr><td><b>'. __("Total Cost ", 'order-now' ).'</b></td><td><b>'.$currency.$ordertotalamt.'</b></td></tr>';
			}
		}
	}
	else if($customer_choice!="")
	{
		$items.='<tr><td colspan="2" align="center" bgcolor="#d6d6d6">'. __("ITEM DETAILS ", 'order-now' ).'</td></tr>';
		$items.='<tr><td>'. __("Customer Choice ", 'order-now' ).'</td><td>'.$customer_choice.'</td></tr>';
	}
	$message.=$items;
	if($mail_content_admin=="")
	{
		$message_team="Hello Team, <br /><br />We have received new order and please analyse this and send confirmation mail asap. <br /><br />";
	}
	else
	{
		$message_team =$mail_content_admin.'<br /><br />';
	}
	$message_team.=$items;
	$subject_team="Order Received from ".$order_heading;
	wp_mail( $to, $subject_team, $message_team, $headers );
	if ( wp_mail( $customer_email, $subject, $message, $headers_customer ) )
	{
		echo '<div align="center" style="padding-top:15px;">';
		if($thanks_msg=="")
		echo '<p>Thanks for contacting us, you can expect a response soon.</p>';
		else
		echo '<p>'.$thanks_msg.'</p>';
		echo '</div>';
	}
	else 
	{
		echo 'An unexpected error occurred';
	}	
die();
}
add_action( 'wp_ajax_category_ajax_request', 'category_ajax_request' );
add_action( 'wp_ajax_nopriv_category_ajax_request', 'category_ajax_request' );
add_action( 'wp_ajax_option_ajax_request', 'option_ajax_request' );
add_action( 'wp_ajax_nopriv_option_ajax_request', 'option_ajax_request' );
add_action( 'wp_ajax_sub_category_details_ajax_request', 'sub_category_details_ajax_request' );
add_action( 'wp_ajax_nopriv_sub_category_details_ajax_request', 'sub_category_details_ajax_request' );
add_action( 'wp_ajax_get_first_category_ajax_request', 'get_first_category_ajax_request' );
add_action( 'wp_ajax_nopriv_get_first_category_ajax_request', 'get_first_category_ajax_request' );
add_action( 'wp_ajax_get_currency_ajax_request', 'get_currency_ajax_request' );
add_action( 'wp_ajax_nopriv_get_currency_ajax_request', 'get_currency_ajax_request' );
add_action( 'wp_ajax_coupon_ajax_request', 'coupon_ajax_request' );
add_action( 'wp_ajax_nopriv_coupon_ajax_request', 'coupon_ajax_request' );
add_action( 'wp_ajax_innerpage_ajax_request', 'innerpage_ajax_request' );
add_action( 'wp_ajax_nopriv_innerpage_ajax_request', 'innerpage_ajax_request' );
add_action( 'wp_ajax_order_form_ajax_request', 'order_form_ajax_request' );
add_action( 'wp_ajax_nopriv_order_form_ajax_request', 'order_form_ajax_request' );
add_action( 'wp_ajax_order_form_ajax_deliver_mail', 'order_form_ajax_deliver_mail' );
add_action( 'wp_ajax_nopriv_order_form_ajax_deliver_mail', 'order_form_ajax_deliver_mail' );
function order_now_menu(){
	$page_title = 'Responsive Order Form';
	$menu_title = 'Order Now';
	$capability = 'manage_options';
	$menu_slug  = 'order_now_menu';
	$function   = 'order_now';
	$icon_url   = 'dashicons-plus-alt';
	$position   = 4;
	add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
	add_submenu_page( 'order_now_menu', 'Order Form Categories', 'Categories', 'manage_options','edit.php?post_type=form_category');
	add_submenu_page( 'order_now_menu', 'Order Form Sub Categories', 'Sub Categories', 'manage_options','edit.php?post_type=form_sub_category');
	add_submenu_page( 'order_now_menu', 'Order Form Options', 'Category Options', 'manage_options', 'edit.php?post_type=form_options');
	add_submenu_page( 'order_now_menu', 'Order Form Settings', 'Settings', 'manage_options',  'order-plugin-settings', 'mt_settings_page');
 }
?>
