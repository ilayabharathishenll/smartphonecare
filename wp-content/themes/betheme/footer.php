<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


$back_to_top_class = mfn_opts_get('back-top-top');

if( $back_to_top_class == 'hide' ){
	$back_to_top_position = false;
} elseif( strpos( $back_to_top_class, 'sticky' ) !== false ){
	$back_to_top_position = 'body';
} elseif( mfn_opts_get('footer-hide') == 1 ){
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}

?>

<?php do_action( 'mfn_hook_content_after' ); ?>

<!-- #Footer -->		
<footer id="Footer" class="clearfix">
	
	<?php if ( $footer_call_to_action = mfn_opts_get('footer-call-to-action') ): ?>
	<div class="footer_action">
		<div class="container">
			<div class="column one column_column">
				<?php echo do_shortcode( $footer_call_to_action ); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<?php 
		$sidebars_count = 0;
		for( $i = 1; $i <= 5; $i++ ){
			if ( is_active_sidebar( 'footer-area-'. $i ) ) $sidebars_count++;
		}
		
		if( $sidebars_count > 0 ){
			
			$footer_style = '';
				
			if( mfn_opts_get( 'footer-padding' ) ){
				$footer_style .= 'padding:'. mfn_opts_get( 'footer-padding' ) .';';
			}
			
			echo '<div class="widgets_wrapper" style="'. $footer_style .'">';
				echo '<div class="container">';
						
					if( $footer_layout = mfn_opts_get( 'footer-layout' ) ){
						// Theme Options

						$footer_layout 	= explode( ';', $footer_layout );
						$footer_cols 	= $footer_layout[0];
		
						for( $i = 1; $i <= $footer_cols; $i++ ){
							if ( is_active_sidebar( 'footer-area-'. $i ) ){
								echo '<div class="column '. $footer_layout[$i] .'">';
									dynamic_sidebar( 'footer-area-'. $i );
								echo '</div>';
							}
						}						
						
					} else {
						// Default - Equal Width
						
						$sidebar_class = '';
						switch( $sidebars_count ){
							case 2: $sidebar_class = 'one-second'; break;
							case 3: $sidebar_class = 'one-third'; break;
							case 4: $sidebar_class = 'one-fourth'; break;
							case 5: $sidebar_class = 'one-fifth'; break;
							default: $sidebar_class = 'one';
						}
						
						for( $i = 1; $i <= 5; $i++ ){
							if ( is_active_sidebar( 'footer-area-'. $i ) ){
								echo '<div class="column '. $sidebar_class .'">';
									dynamic_sidebar( 'footer-area-'. $i );
								echo '</div>';
							}
						}
						
					}
				
				echo '</div>';
			echo '</div>';
		}
	?>


	<?php if( mfn_opts_get('footer-hide') != 1 ): ?>
	
		<div class="footer_copy">
			<div class="container">
				<div class="column one">

					<?php 
						if( $back_to_top_position == 'copyright' ){
							echo '<a id="back_to_top" class="button button_js" href=""><i class="icon-up-open-big"></i></a>';
						}
					?>
					
					<!-- Copyrights -->
					<div class="copyright">
						<?php 
							if( mfn_opts_get('footer-copy') ){
								echo do_shortcode( mfn_opts_get('footer-copy') );
							} else {
								echo '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. <a target="_blank" rel="nofollow" href="http://muffingroup.com">Muffin group</a>';
							}
						?>
					</div>
					
					<?php 
						if( has_nav_menu( 'social-menu-bottom' ) ){
							mfn_wp_social_menu_bottom();
						} else {
							get_template_part( 'includes/include', 'social' );
						}
					?>
							
				</div>
			</div>
		</div>
	
	<?php endif; ?>
	
	
	<?php 
		if( $back_to_top_position == 'footer' ){
			echo '<a id="back_to_top" class="button button_js in_footer" href=""><i class="icon-up-open-big"></i></a>';
		}
	?>

	
</footer>

</div><!-- #Wrapper -->

<?php 
	// Responsive | Side Slide
	if( mfn_opts_get( 'responsive-mobile-menu' ) ){
		get_template_part( 'includes/header', 'side-slide' );
	}
?>

<?php
	if( $back_to_top_position == 'body' ){
		echo '<a id="back_to_top" class="button button_js '. $back_to_top_class .'" href=""><i class="icon-up-open-big"></i></a>';
	}
?>

<?php if( mfn_opts_get('popup-contact-form') ): ?>
	<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php mfn_opts_show( 'popup-contact-form-icon', 'icon-mail-line' ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php echo do_shortcode( mfn_opts_get('popup-contact-form') ); ?>
			<span class="arrow"></span>
		</div>
	</div>
<?php endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>
<?php 
$array_menu = wp_get_nav_menu_items('Smartphonecare');
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['id']      =   $m->ID;
            $menu[$m->ID]['active'] = false;                              
            $menu[$m->ID]['name']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['currentPage'] = false;
            $menu[$m->ID]['isShortcut'] = false;         
            //$menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->id] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['active'] = false;                              
            $submenu[$m->ID]['name']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $submenu[$m->ID]['currentPage'] = false;
            $submenu[$m->ID]['isShortcut'] = false;            
            $menu[$m->menu_item_parent]['children'][] = $submenu[$m->ID];
        }
    }

?>
<script>
        
            window.__INITIAL_STATE__ = {}
            window.__INITIAL_STATE__.menu = {"navigation":<?php echo json_encode( array_values($menu) );?>,"shortcuts":{"bar":[],"list":[]},"sections":{"navigation":{"id":11,"title":"Menu","icon":"Hamburger","order":1}},"business":false};
        

    window.menuConfig = {
        apiHost: '<?php echo site_url();?>'
    };
     var site_url = '<?php echo site_url();?>';


     

    jQuery(document).ready(function() {
	  checkContainer();
	});

	function checkContainer () {
	  if(jQuery('#tredk-menu').html()!=''){
	  	/*jQuery('#tredk-menu').find('li.sectionItem---QQPZD').before('<li class="link-item"><a class="href-link" href="http://minrep.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li>');*/
	  	jQuery('#tredk-menu').find('li.sectionItem---QQPZD').after('<li class="link-item contact-item"><a class="contact-link" href="http://smartphonecare.dk/kontakt/"><img src="'+site_url+'/wp-content/themes/betheme/images/call.png" class="contact-icon"></a></li><li class="link-item mob-menu-items"><a class="href-link" href="http://minrep.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li>');
	  	jQuery('#tredk-menu').find('section.sections---3gZVz').after('<section class="sections---3gZVz search-section"><ul class="none---1EJzg"><li class="sectionItem---QQPZD"><a class="sectionLink---1e5Tr link---22ejX desktop-href-link" href="http://minrep.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li></ul></section>');


	  	/*if(jQuery.browser.mobile){
	  		console.log('mobile');
	  		if(jQuery('.link-item').length == 0){
	  			console.log('mobile12');
		  		
		  	}
	  	}else{
	  		console.log('desk');
	  		if(jQuery('.desktop-href-link').length == 0){
	  			console.log('desktop');
		  		//if the container is visible on the page
		    	jQuery('#tredk-menu').find('section.sections---3gZVz').after('<section class="sections---3gZVz search-section"><ul class="none---1EJzg"><li class="sectionItem---QQPZD"><a class="sectionLink---1e5Tr link---22ejX desktop-href-link" href="http://minrep.smartphonecare.dk/"><img src="'+site_url+'/wp-content/themes/betheme/images/status.png" class="search-icon"><span>Status</span></a></li></ul></section>');
		    }
	  	}*/
	   
	  } else {
	    setTimeout(checkContainer, 50); //wait 50 ms, then try again
	  }
	}
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/smartphone-menu.js"></script>
<!-- wp_footer() -->
<?php wp_footer(); ?>

<style type="text/css">
	.contact-link{
		text-align:center;
		display: block;
	}
	.contact-item{
		background:#218ECE;
	}
	.link-item{
		display: none;
	}
	#Top_bar .top_bar_right_wrapper{
		top:11px;
	}
	#Top_bar .top_bar_right, .header-plain #Top_bar .top_bar_right{
		height: 65px;
	}
	#Top_bar .menu > li > a{
		padding: 3px 0;
	}
	#Top_bar #logo, .header-fixed #Top_bar #logo, .header-plain #Top_bar #logo, .header-transparent #Top_bar #logo{
		padding: 3px 0;
	}
	#Top_bar.is-sticky{
		top:-6px !important;
	}
	.sectionItem---QQPZD, .link-item{
		margin-top: 5px;
	}
	@media only screen and (min-width: 768px){
		.search-section{
			display: block;
		}
		.activeSectionLink---2Z3s4{
			/*border-bottom:3px solid #218ECE !important;*/
			border-left-color: transparent;
			border-right-color: transparent;
		}
		.sectionLink---1e5Tr{
			color:#fff;
			opacity: 1;
		}
		.sectionLink---1e5Tr span{
			margin-bottom: 13px;
    		margin-top: 5px;
		}
		.sectionItem---QQPZD{
			margin-left: 15px;
			margin-right: 15px;
			border-bottom:2px solid #218ECE !important;
		}
		.sectionLink---1e5Tr img, .href-link img {
		    width: 1em;
		    height: 1em;
		    margin-bottom: 6px;
    		margin-top: 16px;
		}
		.sectionItem---QQPZD:first-child{
			margin-top: 5px !important
		}
		.search-section{
			margin-top:10px;
		}
		
	}
	@media only screen and (max-width: 767px){
		.link-item{
			display: block;
		}
		.search-section{
			display: none;
		}
		.sectionLink---1e5Tr, .href-link{
		    padding: 18px 0;
		    color:#fff;
		    opacity: 1;
		}
		.sectionLink---1e5Tr:active, .sectionLink---1e5Tr:focus, .sectionLink---1e5Tr:hover{
			text-decoration: none !important;
		}
		.logo---F1oJ0 a{
			height: calc(100%) !important;
		}
		.activeSectionLink---2Z3s4, .activeSectionLink---2Z3s4:active, .activeSectionLink---2Z3s4:focus{
			border-bottom-color:transparent;
			text-decoration: none !important;
		}
		.sectionLink---1e5Tr > span{
			padding-top: 5px;
		}
		/*.mob-menu-items{
		    border-bottom: 3px solid #218ECD;
		    margin-left: 50px;
		    margin-right: 50px;
		}*/
		ul.none---1EJzg span{
			border-bottom:2px solid #218ECE;
			padding-bottom:3px;
		}
		.contact-item{
			height: auto;
			margin: 5px 80px;
		}
		.contact-link{
			margin-top: 11px;
		}
		.sectionItem---QQPZD, .mob-menu-items{
			margin-top:20px;
		}
		.sectionLink---1e5Tr img, .href-link img {
		    width: 1em;
		    height: 1em;
		    margin-bottom: 5px;
		}
		li.item---1Xn0z span{
			border:none;
		}
		.link-item{
			opacity: 1;
			--webkit-transform: translateY(0);
			transform: translateY(0);
		}
		.mobile-header-mini.mobile-mini-ml-ll #Top_bar .logo{
		 margin-left:0 !important;
		}
		.mobile-header-mini #Top_bar .logo{
		 float:none !important;
		}
		.responsive-menu-toggle {
			display: none !important;
		}
		#Footer {
	 		padding-bottom:25vw;
		}
	}
</style>

</body>
</html>